import 'package:flutter/material.dart';
import '../screens/Filter_Screen.dart';
class mainDrawer extends StatelessWidget {
  Widget buildListTile(String title,IconData icon,String routeName,BuildContext context){
    return ListTile(
      leading:Icon(icon,size: 26,),
      title: Text(title,style:TextStyle(
        fontSize: 24,
        fontFamily:'RobotoCondensed',
        fontWeight: FontWeight.bold,
      ),
      ),
      onTap: ()=>Navigator.of(context).pushReplacementNamed(routeName),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            color: Theme.of(context).accentColor,
            alignment: Alignment.centerLeft,
            child: Text(
              "Cookink Up!",
              style: TextStyle(
                  color: Theme.of(context).primaryColor, fontSize: 30,fontWeight: FontWeight.w900, fontFamily: "Raleway"),
            ),
          ),
          SizedBox(height: 20,),
          buildListTile('Meals',Icons.restaurant,'/',context),
          buildListTile('Filters',Icons.settings,FilterScreen.routeName,context),
        ],
      ),
    );
  }
}
