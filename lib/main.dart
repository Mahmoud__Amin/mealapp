import 'package:flutter/material.dart';
import 'package:mealapp/dummy_data.dart';
import 'package:mealapp/screens/Filter_Screen.dart';
import 'package:mealapp/screens/Tabs_Screen.dart';
import '../screens/category_meals-screen.dart';
import '../screens/categoryscreen.dart';
import '../screens/meal_detail_screen.dart';
import 'models/meal.dart';
void main() {
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String,bool?> _filters ={
    'gluten':false,
    'lactose':false,
    'vegan':false,
    'vegetarian':false,
  };
  List<Meal> _availableMeal=DUMMY_MEALS;
  void _setfilters(Map<String,bool?> _filterData) {
      setState(() {
        _filters=_filterData;

        _availableMeal=DUMMY_MEALS.where((meal){
           if(_filters['gluten']==true&&meal.isGlutenFree==false)return false;
           if(_filters['lactose']==true&&meal.isLactoseFree==false)return false;
           if(_filters['vegan']==true&&meal.isVegan==false)return false;
           if(_filters['vegetarian']==true&&meal.isVegetarian==false)return false;
           return true;

        }).toList();
      });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      routes:{
        '/':(context)=>TabsScreen(),
        CategoryMealsScreen.routeName:(context)=>CategoryMealsScreen(_availableMeal),
        MealDetailScreen.routeName:(context)=>MealDetailScreen(),
        FilterScreen.routeName:(context)=>FilterScreen(_filters,_setfilters),
      },
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor:Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        textTheme: ThemeData.light().textTheme.copyWith(
          bodyText1:TextStyle(
            color:Color.fromRGBO(20, 50, 50, 1)
          ),
          bodyText2:TextStyle(
              color:Color.fromRGBO(20, 50, 50, 1)
          ),
          subtitle1:TextStyle(
            fontSize: 20,
            fontFamily: 'RobotoCondensed',
            fontWeight:FontWeight.bold,
          )
        )
      ),
      //home: MyHomePage() ,
     // home: CategoriesScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
@override
  Widget build(BuildContext context) {

    return Scaffold(
     appBar: AppBar(
        title: Text("Meal App"),
      ),
      body:CategoriesScreen(),
    );
  }
}
