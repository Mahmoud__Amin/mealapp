import 'package:flutter/material.dart';
import 'package:mealapp/widgets/main_Drawer.dart';

class FilterScreen extends StatefulWidget {
  static const routeName = 'filterscreen';
  final Function saveFilters;
  final Map<String,bool?> currentFilters;
  FilterScreen(this.currentFilters,this.saveFilters);
  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  bool? _glutenFree = false;
  bool?_vegan = false;
  bool? _vegetarian = false;
  bool? _lactoseFree = false;
   initState(){
     _glutenFree =widget.currentFilters['gluten'];
     _vegan =widget.currentFilters['vegan'];
     _vegetarian = widget.currentFilters['vegetarian'];
     _lactoseFree =widget.currentFilters['lactose'];
    super.initState();
  }

  Widget buildSwitchListTile(
    String title,
    bool curentValue,
    String subtitle,
    Function(bool value) onChanged,
  ) {
    return SwitchListTile(
        title: Text(
          title,
          style: TextStyle(fontSize: 25),
        ),
        value: curentValue,
        subtitle: Text(subtitle),
        onChanged: onChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Filters"),
        actions: [
          IconButton(
            onPressed: (){
              final selectedFilters={
                 'gluten':_glutenFree,
                 'lactose':_lactoseFree,
                 'vegan':_vegan,
                 'vegetarian':_vegetarian,
                 };
                widget.saveFilters(selectedFilters);
              },
            icon:Icon(Icons.save),
          )
        ],
      ),
      body: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Text(
            "Adjust Your meal Selection.",
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        Expanded(
          child: ListView(
            children: [
              buildSwitchListTile(
                  "Gluten_free", _glutenFree!, "Only Include Gluten_free Meals",
                  (bool newValue) {
                setState(() {
                  _glutenFree = newValue;
                });
              }),
              buildSwitchListTile(
                "Lactose_free",
                _lactoseFree!,
                "Only Include Lactose_free Meals",
                (bool newValue) {
                  setState(() {
                    _lactoseFree = newValue;
                  });
                },
              ),
              buildSwitchListTile(
                  "Vegetarian", _vegetarian!, "Only Include Vegetarian Meals",
                  (bool newValue) {
                setState(() {
                  _vegetarian = newValue;
                });
              }),
              buildSwitchListTile("Vegan", _vegan!, "Only Include vegan Meals",
                  (bool newValue) {
                setState(() {
                  _vegan = newValue;
                });
              }),
            ],
          ),
        )
      ]),
      drawer: mainDrawer(),
    );
  }
}
