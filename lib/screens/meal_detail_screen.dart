import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mealapp/models/meal.dart';
import '../dummy_data.dart';

class MealDetailScreen extends StatefulWidget {
  static const routeName = 'mealdetail';

  @override
  _MealDetailScreenState createState() => _MealDetailScreenState();
}

class _MealDetailScreenState extends State<MealDetailScreen> {
  @override
  Widget build(BuildContext context) {
    final SelectedMealId = ModalRoute.of(context)!.settings.arguments as String;
    final SelectedMealDetails =
        DUMMY_MEALS.firstWhere((meal) => meal.id == SelectedMealId);
    return Scaffold(
      appBar: AppBar(
        title: Text(SelectedMealDetails.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 300,
              width: double.infinity,
              child:
                  Image.asset(SelectedMealDetails.imageUrl, fit: BoxFit.cover),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
               child: Text(
              "Ingredients",
              style: Theme.of(context).textTheme.subtitle1,
            )),
            Container(
              decoration:BoxDecoration(
                color: Colors.white,
                border:Border.all(color:Colors.grey),
                borderRadius:BorderRadius.circular(10),
              ),
              margin:EdgeInsets.all(10),
              padding:EdgeInsets.all(10),
              height: 150,
              width: 300,
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    color:Theme.of(context).accentColor,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 10),
                      child: Text(SelectedMealDetails.ingredients[index]),
                    ),
                  );
                },
                itemCount: SelectedMealDetails.ingredients.length,
              ),
            ),
            Container(
                child: Text(
                  "Steps",
                  style: Theme.of(context).textTheme.subtitle1,
                )),
            Container(
              decoration:BoxDecoration(
                color: Colors.white,
                border:Border.all(color:Colors.grey),
                borderRadius:BorderRadius.circular(10),
              ),
              margin:EdgeInsets.all(10),
              padding:EdgeInsets.all(10),
              height: 150,
              width: 300,
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      ListTile(
                        leading: CircleAvatar(
                          child:Text("# ${index+1}"),
                        ),
                        title: Text(SelectedMealDetails.steps[index]),
                      ),
                      Divider(),
                    ],
                  );
                },
                itemCount:SelectedMealDetails.steps.length,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton:FloatingActionButton(
        child: Icon(SelectedMealDetails.IsItFavorite==false?Icons.star_border_purple500_outlined:Icons.star),
        onPressed: () {
          setState(() {
            SelectedMealDetails.IsItFavorite=!SelectedMealDetails.IsItFavorite;
          });
        },
      ),
    );
  }
}
