import 'package:flutter/material.dart';
import 'package:mealapp/screens/favoritesScreen.dart';
import 'package:mealapp/widgets/main_Drawer.dart';
import 'categoryscreen.dart';
class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  final List Pages = [
    {"page": CategoriesScreen(), 'title': "Categories"},
    {"page": favoritesScreen(), 'title': "Your Favorite"}
  ];
  int selectedIndex = 0;
  void x(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(Pages[selectedIndex]['title']),
        ),
        body: Pages[selectedIndex]['page'],
        bottomNavigationBar: BottomNavigationBar(
          onTap: x,
          backgroundColor: Theme.of(context).primaryColor,
          selectedItemColor: Theme.of(context).accentColor,
          unselectedItemColor: Colors.white,
          currentIndex: selectedIndex,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.category), label: "Categories"),
            BottomNavigationBarItem(
                icon: Icon(Icons.star), label: "Favorites"),
          ],
        ),
        drawer: mainDrawer(),
    );
  }
}
