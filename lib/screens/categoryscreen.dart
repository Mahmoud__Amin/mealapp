import 'package:flutter/material.dart';
import 'package:mealapp/widgets/showcat.dart';

import '../dummy_data.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView(
          padding: EdgeInsets.all(15),
          children: DUMMY_CATEGORIES
              .map((value) => ShowCat(value.id, value.title, value.color,))
              .toList(),
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
            maxCrossAxisExtent: 300,
            childAspectRatio: 3 / 2,
          )),
    );
  }
}
