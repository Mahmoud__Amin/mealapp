import 'package:flutter/material.dart';
import 'package:mealapp/widgets/meal_Item.dart';

import '../dummy_data.dart';
class favoritesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final categoryMeals = DUMMY_MEALS.where((meal) {
      return meal.IsItFavorite==true;
    }).toList();
    return Scaffold(
           //appBar: AppBar(title:Text(""),),
           body:categoryMeals.length!=0?
           ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            return MealItem(
              id: categoryMeals[index].id,
              imageUrl: categoryMeals[index].imageUrl,
              title: categoryMeals[index].title,
              duration: categoryMeals[index].duration,
              affordability: categoryMeals[index].affordability,
              complexity: categoryMeals[index].complexity,
            );
          },
          itemCount: categoryMeals.length,
        ):
           Center(
             child: Padding(
          padding: const EdgeInsets.all(15),
          child: Text("You have no favorites yet - start adding some!",style:Theme.of(context).textTheme.subtitle1),
        ),
           ));
  }
}

